(function (){
  'use strict';

  var express   = require("express")
    , request   = require("request")
    , endpoints = require("../endpoints")
    , helpers   = require("../../helpers")
    , app       = express()

  app.get("/catalogue/images*", function (req, res, next) {
    var url = endpoints.catalogueUrl + req.url.toString();
    console.log("images url "+url);
    request.get(url)
        .on('error', function(e) { next(e); })
        .pipe(res);
  });

  app.get("/getProducts", function (req, res, next) {
    var x = endpoints.catalogueUrl+"/getProducts" ;//+ req.url.toString();
    console.log("getProducts "+x);
    helpers.simpleHttpRequest(x
     , res, next);
  });
/*
    // Create Card - TO BE USED FOR TESTING ONLY (for now)
    app.post("/cards", function(req, res, next) {
        var options = {
            uri: endpoints.cardsUrl,
            method: 'POST',
            json: true,
            body: req.body
        };
        console.log("Posting Card: " + JSON.stringify(req.body));
        request(options, function(error, response, body) {
            if (error) {
                return next(error);
            }
            helpers.respondSuccessBody(res, JSON.stringify(body));
        }.bind({
            res: res
        }));
    });
 */
//Added dmclough check !!
  app.post("/newProduct", function (req, res, next) {
   // var x = endpoints.catalogueUrl+"/newProduct" ;//+ req.url.toString();
    var options = {
      uri: endpoints.catalogueUrl+"/newProduct",
      method: 'POST',
      json: true,
      body: req.body
    };
    console.log("newProduct index.js message "+options );
    request(options, function(error, response, body) {
      if (error) {
        return next(error);
      }
      helpers.respondSuccessBody(res, JSON.stringify(body));
    }.bind({
      res: res
    }));
    /*helpers.simpleHttpRequest(options
        , res, next);*/

  });

  /*
      // Create Customer - TO BE USED FOR TESTING ONLY (for now)
    app.post("/register", function(req, res, next) {
        var options = {
            uri: endpoints.registerUrl,
            method: 'POST',
            json: true,
            body: req.body
        };

   */

  app.get("/tags", function(req, res, next) {
    helpers.simpleHttpRequest(endpoints.tagsUrl, res, next);
  });

  module.exports = app;
}());
