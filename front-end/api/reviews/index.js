(function (){
  'use strict';

  var async     = require("async")
    , express   = require("express")
    , request   = require("request")
    , endpoints = require("../endpoints")
    , helpers   = require("../../helpers")
    , app       = express()


  app.get("/getReviews", function (req, res, next) {
    var x = endpoints.reviewUrl+"/getReviews" ;//+ req.url.toString();
    console.log("getReviews "+x);
    helpers.simpleHttpRequest(x
     , res, next);
  });

  app.get("/cards", function(req, res, next) {
    helpers.simpleHttpRequest(endpoints.cardsUrl, res, next);
  });

  app.post("/newReview", function (req, res, next) {
    console.log("review called");
      var options = {
      uri: endpoints.reviewUrl+"/newReview",
      method: 'POST',
      json: true,
      body: req.body
    };
    console.log("newProduct index.js message "+options );
    request(options, function(error, response, body) {
      if (error) {
        return next(error);
      }
      helpers.respondSuccessBody(res, JSON.stringify(body));
    }.bind({
      res: res
    }));
  });

  module.exports = app;
}());
