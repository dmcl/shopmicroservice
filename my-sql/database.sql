DROP DATABASE IF EXISTS shop;
CREATE DATABASE IF NOT EXISTS shop;
use shop;
CREATE TABLE IF NOT EXISTS products (
         productID    INT UNSIGNED  NOT NULL AUTO_INCREMENT,
         name         VARCHAR(30)   NOT NULL DEFAULT '',
         quantity     INT UNSIGNED  NOT NULL DEFAULT 0,
         price        DECIMAL(7,2)  NOT NULL DEFAULT 99999.99,
         image        VARCHAR(30)   NOT NULL DEFAULT '',
         PRIMARY KEY  (productID)
       );
INSERT INTO products (name, quantity, price, image) VALUES
         ('Car 1', 10000, 0.48,'car1.jpeg'),
         ('Car 2', 8000, 0.49,'car2.jpeg');
         INSERT INTO products (name, quantity, price, image) VALUES
                  ('Car 5', 100, 0.22,'car4.jpeg'),
                  ('Car 6', 80, 0.33,'car3.jpeg');

CREATE TABLE User (
    userID    INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    name VARCHAR(40) NOT NULL,
    password VARCHAR(40) NOT NULL,
    address VARCHAR(60),
    admin VARCHAR(60),
    PRIMARY KEY  (userID)
   
);
CREATE TABLE Review (
    reviewID    INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    userID VARCHAR(40) NOT NULL,
    productID VARCHAR(40) NOT NULL,
    review VARCHAR(200) NOT NULL,
    PRIMARY KEY  (reviewID)
);

INSERT INTO User (name, password, address, admin) VALUES
         ('joe', 'joe', 'cork', 'yes'),
         ('mary', 'mary', 'dublin', 'no');

CREATE TABLE Orders (
    orderID INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    userID INT UNSIGNED  NOT NULL,
    saledate VARCHAR(40) NOT NULL,
    PRIMARY KEY  (orderID)

);

CREATE TABLE OrderDetails (
    orderdetailsID INT UNSIGNED  NOT NULL AUTO_INCREMENT,
    orderID INT UNSIGNED  NOT NULL,
    productID INT UNSIGNED  NOT NULL,
    quantity INT UNSIGNED  NOT NULL,
    PRIMARY KEY  (orderdetailsID)

);

