FROM node:7
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD node front-end/server.js && node cart/app.js && node catalogue/app.js && node users/app.js && reviews/app.js && orders/app.js
#CMD node cart/app.js
#CMD node catlogue/app.js
#CMD node users/app.js
#CMD node review/app.js
#CMD node orders/app.js
EXPOSE 8079 3001 3002 3003 3004 3009 
