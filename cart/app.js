var express = require("express")
    , morgan = require("morgan")
    , path = require("path")
    , bodyParser = require("body-parser")

    , app = express();


app.use(morgan('combined'));
app.use(morgan("dev", {}));
app.use(bodyParser.json());

//app.use(morgan("dev", {}));
var cart = [];

app.post("/add", function (req, res, next) {
    var obj = req.body;
    console.log("add ");
    console.log("Attempting to add to cart: " + JSON.stringify(req.body));

    var max = 0;
    var ind = 0;

    if (cart["" + obj.custId] === undefined)
        cart["" + obj.custId] = [];

    var c = cart["" + obj.custId];

    for (ind = 0; ind < c.length; ind++)
        if (max < c[ind].cartid)
            max = c[ind].cartid;
    var cartid = max + 1;

    var data = {
        "cartid": cartid,
        "productID": obj.productID,
        "name": obj.name,
        "price": obj.price,
        "image": obj.image,
        "quantity": obj.quantity
    };
    var duplicate = 0;
    var index = 0;
    for (i = 0; i < c.length; i++) {
        if (c[i].productID == obj.productID) {
            console.log("<<<<<<<<<<<<<<<<<<<duplicate found>>>>>>>>>>>>" + i);
            duplicate = 1;
            index = i;
            break;
        }
       }
    if (duplicate == 0) {

        c.push(data);
    } else {
        var result = (parseInt(obj.quantity)) + (parseInt(c[index].quantity));
        c[index].quantity = result;
    }
    console.log(cart[1]);
    res.status(201);
    res.send("");

});

/* toDO */
app.delete("/cart/:custId/items/:id", function (req, res, next) {
    var body = '';
    console.log("CART MS")
    console.log("Delete item from cart: for custId " + req.url + ' ' +
        req.params.id.toString());
    console.log("-----------------------------------");
    var i = 0;
    c = Object.values(cart);

    for (i = 0; i < c[0].length; i++) {
        if (c[0][i].cartid == req.params.id){
         console.log("deleted item found");
         c[0].splice(i, 1);
         }
     }
    Object.assign(cart,c);
    res.send(' ');
});

app.get("/cart/:custId/items", function (req, res, next) {

    var custId = req.params.custId;
    console.log("getCart" + custId);
    console.log('custID ' + custId);
    console.log(JSON.stringify(cart["" + custId], null, 2));
    res.send(JSON.stringify(cart["" + custId]));
    console.log("cart sent");

});

var server = app.listen(process.env.PORT || 3003, function () {
    var port = server.address().port;
    console.log("App now running in %s mode on port %d", app.get("env"), port);
});
