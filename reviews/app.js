var http = require('http'),
    url = require('url');

var mysql = require('mysql');

var db = mysql.createConnection({
    host:     'localhost',
    user:     'root',
    password: 'tester',
    database: 'shop'
});
var cart = [];
var theuser=null;
var theuserid =null;
var server = http.createServer(function (request, response) {
    var path = url.parse(request.url).pathname;
    var url1 = url.parse(request.url);
    console.log("path: "+path);
    console.log("url: "+url1)
    console.log("method:" + request.method);
    if (request.method == 'GET') {
        switch (path) {

            case "/getReviews":
                console.log("Test the order micro-service get orders called");
                response.writeHead(200, {
                    'Content-Type': 'text/html',
                    'Access-Control-Allow-Origin': '*'
                });
                var query = "SELECT * FROM Review ";

                db.query(
                    query,
                    [],
                    function(err, rows) {
                        if (err) throw err;
                        console.log(JSON.stringify(rows, null, 2));
                        response.end(JSON.stringify(rows));
                        console.log("Reviews sent");
                    }
                );

                break;

        } //switch
    } else  if (request.method == 'POST') {
        switch (path) {
            case "/newReview":
                var body = '';
                console.log("user review local post");
                request.on('data', function (data) {
                    body += data;
                });
                request.on('end', function () {
                    var review = decodeURIComponent(body.toString());
                    review = review.replace(/\\/g, "");
                    review = review.replace(/[{()}]/g, '');
                    review = review.replace(/['"]+/g, '');
                    review = review.replace(/\\/g, "");
                    review = review.split('+').join(' ');
                    review = review.substr(3);

                    var array = review.split(",");
                    var user = array[0].split(":");
                    var prod = array[1].split(":");
                    var rev  = array[2].split(":");

                    var query = "SELECT * FROM Review where review='"+rev[1]+"'";
                    response.writeHead(200, {
                        'Access-Control-Allow-Origin': '*'
                    });

                    db.query(
                        query,
                        [],
                        function(err, rows) {
                            if (err) {
                                response.end("error");
                                throw err;
                            }
                            if (rows!=null && rows.length>0) {
                                console.log(" user already in database");
                                response.end('{"error": "2"}');
                            }
                            else{
                                query = "INSERT INTO Review (userID, productID, review)"+
                                    "VALUES(?, ?, ?)";
                                db.query(
                                    query,
                                    [user[1],prod[1],rev[1]],
                                    function(err, result) {
                                        if (err) {
                                            // 2 response is an sql error
                                            response.end('{"error": "3"}');
                                            throw err;
                                        }
                                        theuserid = result.insertId;
                                        var obj = {
                                            id: theuserid
                                        }
                                        response.end(JSON.stringify(obj));

                                    }
                                );
                            }
                        }
                    );
                });
                break;

        }
    }
});
server.listen(3009);

